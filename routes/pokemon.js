var express = require('express');
var router = express.Router();
const pokemonController=require('../controllers/pokemon');
/* GET jugadores listing. */

//HTTP get server/jugadores/
router.get('/',pokemonController.getAll);

/* GET jugador por su id. */
router.get('/:id', pokemonController.getOne);
//
////HTTP post server/jugadores/
router.post('/', pokemonController.create);
//
////HTTP put server/jugadores/45
router.put('/:id', pokemonController.update);
//
////HTTP delete server/jugadores/45
router.delete('/:id', pokemonController.destroy);

module.exports = router;