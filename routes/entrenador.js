var express = require('express');
var router = express.Router();
const entrenadoresController=require('../controllers/entrenador');
/* GET jugadores listing. */

//HTTP get server/jugadores/
router.get('/',entrenadoresController.getAll);

/* GET jugador por su id. */
router.get('/:id', entrenadoresController.getOne);
//
////HTTP post server/jugadores/
router.post('/', entrenadoresController.create);
//
////HTTP put server/jugadores/45
router.put('/:id', entrenadoresController.update);
//
////HTTP delete server/jugadores/45
router.delete('/:id', entrenadoresController.destroy);

module.exports = router;
