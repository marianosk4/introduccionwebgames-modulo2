const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const entrenador = new Schema ({
    name: String,
    medals:Number,
    pokemones: [{type: Schema.Types.ObjectId, ref:'Pokemon'}],
    createdAt: {type: Date, default:Date.now},
});

const Entrenador = mongoose.model('Entrenador', entrenador, 'entrenador');

module.exports = Entrenador;