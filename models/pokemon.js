const mongoose=require('mongoose');
const Schema = mongoose.Schema;

const pokemon= new Schema({
    id: Number,
    name: String,
    type: [String, String],
    base:{
        HP: Number,
        Attack: Number,
        Defense: Number,
        SpAttack: Number,
        SpDefense: Number,
        Speed: Number,
    },
});

const Pokemon = new mongoose.model('Pokemon', pokemon, 'pokemon');
module.exports = Pokemon;
