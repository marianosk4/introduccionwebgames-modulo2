const mongoose = require('mongoose');
const pokemon = require('./pokemon');
const Entrenador = require('./entrenador');

const connectDb=function(){
    return mongoose.connect('mongodb://localhost:27017/Trainers', {useNewUrlParser:true});
};

const models={
    pokemon,
    Entrenador
};


module.exports = {
    connectDb,
    models
};