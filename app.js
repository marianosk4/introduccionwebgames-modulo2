const createError = require('http-errors');
const express = require('express');
//const path = require('path');
const logger = require('morgan');

const entrenadoresRouter = require('./routes/entrenador');
const pokemonesRouter = require('./routes/pokemon');

const app = express();

// view engine setup
//app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
// app.use(express.urlencoded({ extended: false }));
// app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));

app.use('/entrenadores', entrenadoresRouter);
app.use('/pokemones', pokemonesRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  const error = err.message;

  if(req.app.get('env') === 'development') console.error(err);

  // render the error page
  res.status(err.status || 500).json(error);
});

module.exports = app;
