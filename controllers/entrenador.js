const db = require('../models');
const Entrenador = db.models.Entrenador;



function getAll(req, res, next) {
  
  const name = req.query.name;

  let  find = name ? Entrenador.find({ name: {$regex: name, $options:'i'} }) : Entrenador.find();

  return find.populate('pokemones').then(entrenadores =>{
    return res.status(200).json(entrenadores);
  }).catch(next);
}

function getOne(req, res, next) {
    const id = req.params.id;

    if (!id){
        return res.status(500).json({message: "El id indicado no es un numero"});
    }

    //const jugador = jugadores.find(jugador => jugador.id === email);
 
    return Entrenador.findById(id).then(entrenador =>{
      if (!entrenador){
        return res.status(404).json({message: "No existe name " + name});
      }

      return res.status(200).json(entrenador);
    })
    .catch(next);
  }

function create(req, res ,next){
  const body = req.body;

  const entrenador = {
    name: body.name,
    medals: body.medals,
    pokemones:body.pokemones,
  }
  return Entrenador.create(entrenador).then(entrenadorNuevo=>{
    return res.status(200).json(entrenadorNuevo);
  })
  .catch(next);
} 

function update(req, res ,next){
  const id = req.params.id; 
  const body = req.body;

  const entrenador = {
    name: body.name,
    medals: body.medals,
    pokemones:body.pokemones,
  }
  return Entrenador.findOneAndUpdate({ _id: id },entrenador, { new:true }).then(entrenadorUpdateado=>{
    return res.status(200).json(entrenadorUpdateado);
  })
  .catch(next);
}

function destroy(req, res ,next){
  const id = req.params.id; 
 
  return Entrenador.findByIdAndDelete( id).then(entrenadorDestruido=>{
    return res.status(200).json(entrenadorDestruido);
  })
  .catch(next);
}


  module.exports = {
      getAll,
      getOne,
      create,
      update,
      destroy
    };