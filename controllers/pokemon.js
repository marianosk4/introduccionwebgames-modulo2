const db = require('../models');
const Pokemon = db.models.pokemon;



function getAll(req, res, next) {
  
  const name = req.query.name;

  let  find = name ? Pokemon.find({ name :{ $regex: name, $options:'i'} }) : Pokemon.find();

  return find.then(pokemon =>{
    return res.status(200).json(pokemon);
  }).catch(next);
}

function getOne(req, res, next) {
    const id = req.params.id;

    if (!id){
        return res.status(500).json({message: "El id indicado no es un numero"});
    }

    //const jugador = jugadores.find(jugador => jugador.id === email);
 
    return Pokemon.findById(id).then(pokemon =>{
      if (!pokemon){
        return res.status(404).json({message: "No existe el pokemon " + id });
      }

      return res.status(200).json(pokemon);
    })
    .catch(next);
  }

function create(req, res ,next){
  const body = req.body;

const pokemon= {
    id:body.id,
    name: body.name,
    type: body.type,
    base: body.base,
    HP: body.HP,
    Attack: body.Attack,
    Defense: body.Defense,
    SpAttack: body.SpAttack,
    SpDefense: body.SpDefense,
    Speed: body.Speed,

}
  return Pokemon.create(pokemon).then(pokeNuevo=>{
    return res.status(200).json(pokeNuevo);
  })
  .catch(next);
} 

function update(req, res ,next){
  const id = req.params.id; 
  const body = req.body;

  
  const pokemon= {
    id:body.id,
    name: body.name,
    type: body.type,
    base: body.base,
    HP: body.HP,
    Attack: body.Attack,
    Defense: body.Defense,
    SpAttack: body.SpAttack,
    SpDefense: body.SpDefense,
    Speed: body.Speed,

}
  return Pokemon.findOneAndUpdate({ _id: id },pokemon, { new:true }).then(pokemonUpdateado=>{
    return res.status(200).json(pokemonUpdateado);
  })
  .catch(next);
}

function destroy(req, res ,next){
  const id = req.params.id; 
 
  return Pokemon.findByIdAndDelete(id).then(pokemonDestruido=>{
    return res.status(200).json(pokemonDestruido);
  })
  .catch(next);
}


  module.exports = {
      getAll,
      getOne,
      create,
      update,
      destroy
    };